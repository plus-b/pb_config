<?php

namespace PlusB\PbConfig\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Samir Rachidi <sr@plusb.de>, plusB
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\AbstractController;
use TYPO3\CMS\Extensionmanager\Controller\ConfigurationController;

/**
 *
 */
class PasswordController extends AbstractController
{

    private function getConfigurationUtility()
    {
        return GeneralUtility::makeInstance(\TYPO3\CMS\Extensionmanager\Utility\ConfigurationUtility::class);
    }

    /**
     * Returns the type string for the password type field. It should be
     *
     * 'user[EXT:pb_config/Classes/Controller/PasswordController.php:PlusB\PbConfig\Controller\PasswordController->render]'
     *
     * @return string
     */
    private function getTypeString()
    {
        $reflection = new \ReflectionClass($this);
        $path = $this->getPathByFullNamespaceName($reflection->getName());
        $typeString = sprintf(
            'user[EXT:%s/Classes/%s.php:%s->render]',
            $this->getExtensionKey(),
            $path,
            $reflection->getName()
        );

        return $typeString;
    }

    private function getPathByFullNamespaceName($fullNamespaceName)
    {
        $matches = array();
        $result = preg_match('/([a-zA-Z]*\\\[a-zA-Z]*\\\)([a-zA-Z\\\]*)/m', $fullNamespaceName, $matches);
        return str_replace('\\', '/', $matches[2]);
    }

    /**
     * Returns the extension key of this extension
     *
     * @return string
     */
    private function getExtensionKey()
    {
        return GeneralUtility::camelCaseToLowerCaseUnderscored($this->extensionName);
    }

    /**
     * Renders the configuration form field in the extension configuration
     *
     * ATTENTION: Needs to return html
     * @param array $params
     * @param $pObj
     * @return string
     */
    public function render(array $params, $pObj)
    {
        return sprintf(
            '<input type="password" id="em-%s" name="%s" value="%s">',
            $params['propertyName'],
            $params['fieldName'],
            \PlusB\PbConfig\Service\EncryptionService::decrypt($params['fieldValue'])
        );
    }

    /**
     * Signal slot (see ext_localconf.php) which is invoked after saving the value in the LocalConfiguration.php. We hook into the saving process,
     * encrypt the value and save it again into the LocalConfiguration.php
     *
     * @param $extensionKey
     * @param $configurationArray
     * @param ConfigurationController $configurationController
     */
    public function afterExtensionConfigurationWrite($extensionKey, $configurationArray, \TYPO3\CMS\Extensionmanager\Controller\ConfigurationController $configurationController)
    {
        //Decrypt the password value
        array_walk($configurationArray, function (&$configuration, $configurationKey) {
            if ($configuration['type'] === $this->getTypeString()) {
                $configuration['value'] = \PlusB\PbConfig\Service\EncryptionService::encrypt($configuration['value']);
            }
        });

        //save the password value
        /** @var $configurationUtility \TYPO3\CMS\Extensionmanager\Utility\ConfigurationUtility */
        $configurationUtility = $this->getConfigurationUtility();
        $newConfiguration = $configurationUtility->getCurrentConfiguration($extensionKey);

        \TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule($newConfiguration, $configurationArray);
        $configurationUtility->writeConfiguration(
            $configurationUtility->convertValuedToNestedConfiguration($newConfiguration),
            $extensionKey
        );
    }
}