<?php

namespace PlusB\PbConfig\Service;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Samir Rachidi <sr@plusb.de>, plusB
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 */
class EncryptionService
{

    const CIPHER_METHOD = 'AES256';

    /**
     * Decrypts a value
     *
     * @param $value
     * @return string
     */
    public static function decrypt($value)
    {
        return openssl_decrypt(base64_decode($value), self::CIPHER_METHOD, self::getKey(), OPENSSL_ALGO_SHA1, self::getIV());
    }

    /**
     * Encrypts a value
     *
     * @param $value
     * @return string
     */
    public static function encrypt($value)
    {
        return base64_encode(openssl_encrypt($value, self::CIPHER_METHOD, self::getKey(), OPENSSL_ALGO_SHA1, self::getIV()));
    }

    /**
     * Returns an initialization vector derived from the encryption key
     *
     * @return bool|string
     */
    public static function getIV()
    {
        return substr(self::getKey(), 0, 16);
    }

    /**
     * Returns the encryption key from the LocalConfiguration.php
     *
     * @return string
     */
    public static function getKey()
    {
        return $GLOBALS['TYPO3_CONF_VARS']['SYS']['encryptionKey'];
    }

}