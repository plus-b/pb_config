﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _configuration:

Configuration Reference
=======================

You can easily reuse the encrypted extension manager configuration. Just use the following example in your own ext_conf_template.txt
of your extension

::

# cat=MyCat; type=user[EXT:pb_config/Classes/Controller/PasswordController.php:PlusB\PbConfig\Controller\PasswordController->render]; label=This is my encrypted field;
my_extension_config_field =


The most important part is the *type* value which must be exactly like this:

::

user[EXT:pb_config/Classes/Controller/PasswordController.php:PlusB\PbConfig\Controller\PasswordController->render]

See here for more information regarding custom types:  https://docs.typo3.org/typo3cms/TyposcriptSyntaxReference/TypoScriptTemplates/TheConstantEditor/Index.html#type