﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt

.. _start:

=============================================================
PlusB Download Form with FAL
=============================================================

.. only:: html

	:Classification:
		pb_downloadform

	:Version:
		|release|

	:Language:
		en

	:Description:
		This extension provides a simple way to implement encrypted extension manager configurations

	:Keywords:
		configuration, extensionmanager

	:Copyright:
		2015

	:Author:
		Samir Rachidi

	:Email:
		sr@plusb.de

	:License:
		This document is published under the Open Content License
		available from http://www.opencontent.org/opl.shtml

	:Rendered:
		|today|

	The content of this document is related to TYPO3,
	a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.

	**Table of Contents**

.. toctree::
	:maxdepth: 5
	:titlesonly:
	:glob:

	Introduction/Index
	User/Index
	Administrator/Index
	Configuration/Index
	Developer/Index
	KnownProblems/Index
	ToDoList/Index
	ChangeLog/Index
	Targets
