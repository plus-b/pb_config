﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _introduction:

Introduction
============

With this extension it is possible to implement encrypted configuration parameters for your custom extension.
The value of the extension configuration parameter will be stored encrypted inside LocalConfiguration.php and will be decrypted
on displaying it in the backend. Also, an <input type="password" /> tag will be rendered in the extension configuration view.

.. _what-it-does:

What does it do?
----------------

Adds the possibility to store an extension configuration paramater encrypted inside the LocalConfiguration.php. With the custom type

::

user[EXT:pb_config/Classes/Controller/PasswordController.php:PlusB\PbConfig\Controller\PasswordController->render]

an input field of type "password" will be rendered in the extension configuration view. On save this extension will encrypt the value,
base64 encode it and then save it in the LocalConfiguration.php. The LocalConfiguration.php will e.g. look like this:


::

<?php
return [
    'BE' => [

        //..

    ],
    'DB' => [

        //..

    ],
    'EXT' => [

        'extConf' => [

            //..

            'pb_config' => 'a:1:{s:8:"password";s:44:"I80TJFwwdWyyAygij7Eq/yAdh5hd4vDuwZv7JsyGf4Y=";}',

            //..
        ],
    ],
    'FE' => => [

        //..

    ],
    'SYS' => => [

        //..

    ],
];


What does it NOT do?
--------------------

IT DOES NOT securely encrypt and save your extension configuration parameter inside the LocalConfiguration.php. An attacker can easily restore
the password with the encryption key.

For Developers
--------------

Important: You need to use the service class \PlusB\PbConfig\Service\EncryptionService and its static decrypt() function, to decrypt the values
fetched from the configuration.

Example:

::

<?php

class MyExtbaseController extends AbstractController {

    //...

    /**
    *   show action
    */
    public function showAction()
    {
        //fetch the configuration array of your extension
        $extensionConfiguration = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['pb_config']);
        $password = \PlusB\PbConfig\Service\EncryptionService::decrypt($extensionConfiguration['password']);

    }

    //...

}