﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _known-problems:

Known Problems
==============

Attention: This extension will not provide a secure way to store passwords in the extension configuration, because an attacker
will just need the encryption-key of the typo3 system and some insights of the code. With this, the password can be easily restored.

The purpose of this extension is only to encrypt the password in the LocalConfiguration.php file.
