﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _todo:

To-Do list
==========

- Typo3 needs a "afterExtensionConfigurationRead" Signal-Slot. With this, we could implement this feature fully. Until then developers need to use the static method \PlusB\PbConfig\Service\EncryptionService::decrypt()