<?php
$extensionPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('pb_config');
return array(
    'PlusB\PbConfig\Controller\PasswordController' => $extensionPath . 'Classes/Controller/PasswordController.php',
);